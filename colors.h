#pragma once

#include <QObject>
#include <QPalette>
#include <QMetaEnum>
#include <QGuiApplication>

class PaletteRole
{
    Q_GADGET
    Q_PROPERTY(QString label MEMBER m_label)
    Q_PROPERTY(QColor color MEMBER m_color);

public:
    PaletteRole(){}
    PaletteRole(QPalette::ColorGroup group, QPalette::ColorRole role)
    {
        static QMetaEnum metaGroup = QMetaEnum::fromType<QPalette::ColorGroup>();
        static QMetaEnum metaRole = QMetaEnum::fromType<QPalette::ColorRole>();
        m_label = QString(metaGroup.valueToKey(group)) + " " + QString(metaRole.valueToKey(role));

        static auto palette = QGuiApplication::palette();

        m_color = palette.color(group, role);
    }
private:
    QString m_label;
    QColor m_color;
};

Q_DECLARE_METATYPE(PaletteRole)

class Colors : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector<PaletteRole> roles MEMBER m_roles CONSTANT)
public:

    Colors(QObject *parent = nullptr);

private:
    QVector<PaletteRole> m_roles;
};
