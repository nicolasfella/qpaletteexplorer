#include <QGuiApplication>

#include <QQmlApplicationEngine>

#include "colors.h"

int main(int argc, char** argv)
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qRegisterMetaType<QVector<PaletteRole>>();

    Colors c;
    qmlRegisterSingletonInstance<Colors>("qpaletteexplorer", 1, 0, "Colors", &c);

    engine.load(":/main.qml");

    return app.exec();

}
