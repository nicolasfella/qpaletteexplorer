import QtQuick 2.15
import QtQuick.Controls 2.15

import qpaletteexplorer 1.0

ApplicationWindow {

    visible: true

    ListView {

        anchors.fill: parent

        model: Colors.roles

        delegate: Row {

            Label {
                text: modelData.label
            }

            Rectangle {
                width: 200
                height: 100
                color: modelData.color
            }
        }
    }


}
