#include "colors.h"

Colors::Colors(QObject *parent)
    : QObject(parent)
{

    m_roles << PaletteRole{QPalette::Active, QPalette::Window};
    m_roles << PaletteRole{QPalette::Active, QPalette::Background};
    m_roles << PaletteRole{QPalette::Active, QPalette::WindowText};
    m_roles << PaletteRole{QPalette::Active, QPalette::Foreground};
    m_roles << PaletteRole{QPalette::Active, QPalette::Base};
    m_roles << PaletteRole{QPalette::Active, QPalette::AlternateBase};
    m_roles << PaletteRole{QPalette::Active, QPalette::ToolTipBase};
    m_roles << PaletteRole{QPalette::Active, QPalette::PlaceholderText};
    m_roles << PaletteRole{QPalette::Active, QPalette::Text};
    m_roles << PaletteRole{QPalette::Active, QPalette::Button};
    m_roles << PaletteRole{QPalette::Active, QPalette::ButtonText};
    m_roles << PaletteRole{QPalette::Active, QPalette::BrightText};
    m_roles << PaletteRole{QPalette::Active, QPalette::Highlight};
    m_roles << PaletteRole{QPalette::Active, QPalette::HighlightedText};
    m_roles << PaletteRole{QPalette::Active, QPalette::Link};
    m_roles << PaletteRole{QPalette::Active, QPalette::LinkVisited};
}
